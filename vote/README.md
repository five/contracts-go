This is the vote contract for test net demo.

The description of methods are below:
## 1. issueProject
### args: 
#### key: "projectInfo"
#### value: json
#### example:
```json
{"projectInfo":{"Id":"projectId1","PicUrl":"www.sina.com","Title":"wonderful","StartTime":"1664450291","EndTime":"1665314291","Desc":"the 1","Items":[{"Id":"item1","PicUrl":"www.baidu.com","Desc":"beautiful"},{"Id":"item2","PicUrl":"www.baidu.com","Desc":"beautiful"}]}}
```
### event:
#### topic: issue project
#### data: same as args

## 2. vote
### args:
#### key1: "projectId"
#### value1: string
#### key2: "itemId"
#### value2: string
#### example:
```json
{"projectId":"projectId1","itemId":"item1"}
```
### event:
#### topic: vote
#### data: projectId, itemId, voter
#### example:
```json
["projectId1","itemId1","441224c1757ec1cc67f4b7b3ac29c76cf5799ee5"]
```

## 3. queryProjectVoters
### args:
#### key1: "projectId"
#### value1: string
#### example:
```json
{"projectId":"projectId1"}
```
#### resp exampl:
```json
{"ProjectId":"projectId1","ItemVotes":[{"ItemId":"item1","VotesCount":1,"Voters":["441224c1757ec1cc67f4b7b3ac29c76cf5799ee5"]},{"ItemId":"item2","VotesCount":1,"Voters":["441224c1757ec1cc67f4b7b3ac29c76cf5799ee5"]}]}
```

## 4. queryProjectItemVoters
### args:
#### key1: "projectId"
#### value1: string
#### key2: "itemId"
#### value2: string
#### example:
```json
{"projectId":"projectId1","itemId":"item1"}
```
#### resp example:
```json
{"ItemId":"item1","VotesCount":1,"Voters":["441224c1757ec1cc67f4b7b3ac29c76cf5799ee5"]}
```